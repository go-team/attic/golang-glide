Source: golang-glide
Section: devel
Priority: optional
Maintainer: Debian Go Packaging Team <pkg-go-maintainers@lists.alioth.debian.org>
Uploaders: Vincent Bernat <bernat@debian.org>
Build-Depends: debhelper-compat (= 12),
               dh-golang,
               golang-go,
               golang-github-codegangsta-cli-dev (>= 1.16),
               golang-github-masterminds-semver-dev (>= 1.3),
               golang-github-masterminds-vcs-dev (>= 1.8),
               golang-github-mitchellh-go-homedir-dev,
               golang-gopkg-check.v1-dev,
               golang-gopkg-yaml.v2-dev,
Standards-Version: 4.1.1.0
Homepage: https://github.com/Masterminds/glide
Vcs-Browser: https://salsa.debian.org/go-team/packages/golang-glide
Vcs-Git: https://salsa.debian.org/go-team/packages/golang-glide.git
XS-Go-Import-Path: github.com/Masterminds/glide
Testsuite: autopkgtest-pkg-go

Package: golang-glide
Architecture: any
Built-Using: ${misc:Built-Using}
Depends: ${shlibs:Depends},
         ${misc:Depends},
         git
Breaks: glide (<< 0.12.3-2~)
Replaces: glide (<< 0.12.3-2~)
Description: Vendor package management for Go
 Glide is comparable to tools like Cargo, npm, Composer, Bundler, pip,
 Maven but for Go. It manages vendored packages with ease.
 .
 Packages to be vendored are listed in a configuration file at the
 root of a project along with the appropriate restriction on versions
 and Glide will download the most recent versions satisfying the
 constraints into the vendor directory. It will write a lock file to
 record the exact versions chosen.
